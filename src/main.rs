mod commands;
mod downloader;

use commands::*;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "jukebx", about, author)]
pub struct Jukebx {
    /// The command to run. For specific help about each command, run `jukebx <command> --help`
    #[structopt(subcommand)]
    pub command: Command,
}

fn main() {
    let jukebx = Jukebx::from_args();

    jukebx.command.run();
}
