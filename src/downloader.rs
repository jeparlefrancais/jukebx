use std::io::{self, Write};
use std::path::PathBuf;
use std::process::Command;
use std::sync::mpsc::{channel, Receiver, TryRecvError};

use workerpool::{Pool, Worker};

#[derive(Debug, Clone)]
pub struct Song {
    pub url: String,
    pub file: PathBuf,
    pub title: String,
    pub artist: String,
    pub album_artist: String,
    pub album: String,
    pub number: usize,
    pub year: usize,
    pub bpm: Option<usize>,
}

#[derive(Debug, Clone, Default)]
struct CommandLineWorker {}

fn get_metadata(song: &Song) -> String {
    format!(
        r#"-metadata title="{}" -metadata artist="{}" -metadata album_artist="{}" -metadata album="{}" -metadata date="{}" -metadata track={}"#,
        &song.title, &song.artist, &song.album_artist, &song.album, &song.year, &song.number,
    )
}

impl Worker for CommandLineWorker {
    type Input = Song;
    type Output = Result<(), Song>;

    fn execute(&mut self, song: Self::Input) -> Self::Output {
        let output_file = format!("{}.%(ext)s", song.file.display());
        println!("downloading {} by {}", &song.title, &song.artist);

        let mut command = Command::new("python");
        command.args(&[
            "-m",
            "youtube_dl",
            "--extract-audio",
            "--audio-format",
            "mp3",
            "--audio-quality",
            "320",
            "--postprocessor-args",
            &get_metadata(&song),
            "--output",
            &output_file,
            &song.url,
        ]);

        let output = command.output().map_err(|_| song.clone())?;

        if output.status.success() {
            Ok(())
        } else {
            io::stdout()
                .write_all(&output.stdout)
                .expect("unable to write to stdout");
            io::stderr()
                .write_all(&output.stderr)
                .expect("unable to write to stderr");

            Err(song)
        }
    }
}

pub struct Downloader {
    pool: Pool<CommandLineWorker>,
}

impl Downloader {
    pub fn new() -> Self {
        Self { pool: Pool::new(4) }
    }

    pub fn download_songs(&self, songs: Vec<Song>) -> Vec<Song> {
        let mut receivers: Vec<Receiver<Result<(), Song>>> = songs
            .into_iter()
            .map(|song| {
                let (sender, receiver) = channel();

                self.pool.execute_to(sender, song);

                receiver
            })
            .collect();

        let mut not_downloaded = Vec::new();

        while receivers.len() > 0 {
            let mut i = 0;

            while i != receivers.len() {
                let receiver = &receivers[i];

                let keep = match receiver.try_recv() {
                    Ok(result) => match result {
                        Ok(()) => false,
                        Err(song) => {
                            not_downloaded.push(song);
                            false
                        }
                    },
                    Err(TryRecvError::Disconnected) => false,
                    Err(TryRecvError::Empty) => true,
                };

                if keep {
                    i += 1;
                } else {
                    receivers.remove(i);
                }
            }
        }

        not_downloaded
    }
}
