use crate::downloader::{Downloader, Song};

use std::fs;
use std::path::{Path, PathBuf};
use std::process;

use serde::Deserialize;
use structopt::StructOpt;

#[derive(Debug, Clone, Deserialize)]
struct SongContent {
    pub url: String,
    pub title: String,
    pub artist: Option<String>,
    #[serde(default)]
    pub bpm: Option<usize>,
    #[serde(alias = "#")]
    #[serde(default)]
    pub number: Option<usize>,
}

const REMOVE_CHARS_FOR_FILE_NAME: &[char] = &['/', '\\', ':', '*', '?', '<', '>', '|'];

impl SongContent {
    pub fn get_file_name(&self) -> PathBuf {
        let mut remove_char_indexes = REMOVE_CHARS_FOR_FILE_NAME
            .iter()
            .filter_map(|c| self.title.find(*c));

        if let Some(first_index) = remove_char_indexes.next() {
            let mut file_name = self.title.clone();
            file_name.remove(first_index);

            let mut removed = 1;

            while let Some(index) = remove_char_indexes.next() {
                file_name.remove(index - removed);
                removed += 1;
            }

            file_name.into()
        } else {
            PathBuf::from(&self.title)
        }
    }
}

#[derive(Debug, Clone, Deserialize)]
struct AlbumContent {
    artist: String,
    title: String,
    year: usize,
    songs: Vec<SongContent>,
}

impl AlbumContent {
    pub fn extract_songs(&self, parent: &Path) -> Vec<Song> {
        let album_folder = parent.join(&self.title);

        self.songs
            .iter()
            .enumerate()
            .map(|(i, song)| Song {
                url: song.url.clone(),
                file: album_folder.join(song.get_file_name()),
                title: song.title.clone(),
                artist: song.artist.clone().unwrap_or(self.artist.clone()),
                album_artist: self.artist.clone(),
                album: self.title.clone(),
                number: song.number.unwrap_or(i + 1),
                year: self.year,
                bpm: song.bpm,
            })
            .collect()
    }
}

#[derive(Debug, Clone, Deserialize)]
#[serde(untagged)]
enum DownloadContent {
    Album(AlbumContent),
}

impl DownloadContent {
    pub fn extract_songs(&self, parent: &Path) -> Vec<Song> {
        use DownloadContent::*;

        match self {
            Album(album) => album.extract_songs(parent),
        }
    }
}

#[derive(Debug, StructOpt)]
pub struct Download {
    /// Path to the content file to download.
    #[structopt(parse(from_os_str))]
    input_path: PathBuf,
    /// Where to output the result.
    #[structopt(short = "o", long = "output-path", parse(from_os_str))]
    output_path: Option<PathBuf>,
    #[structopt(short = "f", long = "force-redownload")]
    force_redownload: bool,
    #[structopt(short = "r", long = "recursive")]
    recursive: bool,
}

fn matches_name(file_name: &str) -> bool {
    file_name.ends_with(".jukebx.json")
        || file_name.ends_with(".jukebx")
        || file_name.ends_with(".jukebx.json5")
}

fn extract_songs_from_file(path: &Path, out_path: &Path) -> Vec<Song> {
    let input_file =
        fs::read_to_string(path).expect(&format!("could not read input file {}", path.display()));
    let content: DownloadContent = json5::from_str(&input_file)
        .expect(&format!("could not parse intput file {}", path.display()));

    content.extract_songs(out_path)
}

fn extract_songs_from_directory(path: &Path, out_path: &Path) -> Vec<Song> {
    let dir_content = path
        .read_dir()
        .expect(&format!("unable to read directory {}", path.display()));

    let mut songs = Vec::new();

    dir_content.for_each(|entry| {
        let entry = entry.expect("unable to read directory entry");
        let file_name = entry.file_name();
        let file_name = file_name.to_string_lossy();
        let entry_path = entry.path();

        if entry_path.is_file() && matches_name(&file_name) {
            let file_path = path.join(entry.file_name());
            let out_path = out_path.clone();

            songs.extend(extract_songs_from_file(&file_path, &out_path));
        } else if entry_path.is_dir() {
            let out_path = out_path.join(file_name.as_ref());
            let new_songs = extract_songs_from_directory(&entry_path, &out_path);

            songs.extend(new_songs);
        }
    });

    songs
}

impl Download {
    pub fn run(&self) {
        let mut songs = if self.input_path.is_file() {
            let output = self
                .output_path
                .clone()
                .unwrap_or(self.input_path.parent().unwrap().to_owned());

            extract_songs_from_file(&self.input_path, &output)
        } else {
            let output = self
                .output_path
                .clone()
                .unwrap_or(self.input_path.to_owned());

            if self.recursive {
                extract_songs_from_directory(&self.input_path, &output)
            } else {
                let dir_content = self.input_path.read_dir().expect(&format!(
                    "unable to read directory {}",
                    self.input_path.display()
                ));

                let mut songs = Vec::new();

                dir_content.for_each(|entry| {
                    let entry = entry.expect("unable to read directory entry");
                    let file_name = entry.file_name();
                    let file_name = file_name.to_string_lossy();

                    if entry.path().is_file() && matches_name(&file_name) {
                        let file_path = self.input_path.join(entry.file_name());
                        let out_path = self
                            .output_path
                            .clone()
                            .unwrap_or(self.input_path.to_owned());

                        songs.extend(extract_songs_from_file(&file_path, &out_path));
                    }
                });

                songs
            }
        };

        if !self.force_redownload {
            let mut i = 0;
            while i != songs.len() {
                let song = &songs[i];

                let file_name = PathBuf::from(format!("{}.mp3", song.file.display()));

                if file_name.is_file() {
                    songs.remove(i);
                } else {
                    i += 1;
                }
            }
        }

        let total_songs = songs.len();

        let downloader = Downloader::new();

        let not_downloaded = downloader.download_songs(songs);
        let not_downloaded_count = not_downloaded.len();

        let downloaded_count = total_songs - not_downloaded_count;
        println!(
            "Downloaded {} song{}",
            downloaded_count,
            if downloaded_count > 1 { "s" } else { "" }
        );

        if not_downloaded_count != 0 {
            eprintln!(
                "Unable to download {} song{}",
                not_downloaded_count,
                if not_downloaded_count > 1 { "s" } else { "" }
            );
            not_downloaded.iter().for_each(|song| {
                eprintln!("    {}", song.title);
            });

            process::exit(1);
        }
    }
}
