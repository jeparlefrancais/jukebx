mod download;

pub use download::Download;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub enum Command {
    /// Download content
    Download(Download),
}

impl Command {
    pub fn run(&self) {
        use Command::*;

        match self {
            Download(download) => download.run(),
        }
    }
}
