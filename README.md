# jukebx

A command line tool that download audio files from YouTube using [youtube-dl](https://github.com/ytdl-org/youtube-dl).

## Usage

To get a little bit of help when using the tool, run `jukebx help`. It will show the list of available commands.

You can also run `jukebx help <command name>` to know more about how to use a specific command. For example, try running `jukebx help download`.

## Content File Format

When downloading, jukebx can read files ending with `.jukebx`, `jukebx.json` or `.jukebx.json5` that tells it where to download the content and the metadata associated with it.

```json5
{
    artist: "artist name",
    title: "album title",
    year: 2020,
    songs: [
        {
            url: "https://www.youtube.com/watch?v=somevideo",
            title: "title",
            // the following fields are optional:
            artist: "override artist name",
            bpm: 140,
            #: 99,  // track number (defaults to the position in the album song list)
        },
    ]
}
```
